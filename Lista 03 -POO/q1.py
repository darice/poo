class Pessoa:
    def __init__(self, nome, dataNascimento,altura):
        self._nome = nome;
        self._dataNascimento = dataNascimento;
        self._altura = altura;

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, novoNome):
        self._nome = novoNome

    @property
    def dataNascimento(self):
        return self._dataNascimento

    @dataNascimento.setter
    def dataNascimento(self, novaDataNascimento):
        self._dataNascimento = novaDataNascimento

    @property
    def altura(self):
        return self._altura

    @altura.setter
    def altura(self, novaAltura):
        self._altura = novaAltura

    def imprimirDados(self):
        print(self._nome, self._dataNascimento, self._altura)

    def calcularIdade(self):
        _,_,ano  = self._dataNascimento.split('/')
        ano = int(ano)
        return 2020-ano

