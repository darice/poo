from elevador import Elevador

def menu():
    print()
    print("Escolha uma opcao:")
    print("1-entra")
    print("2-sai")
    print("3-sobe")
    print("4-desce")
    print("5-inicializa")

e = Elevador(10,5)

while(True):
    menu()
    op = int(input())
    if(op == 1):
        n = int(input("Digite a quantidade de pessoas que vão entrar "))
        e.entra(n)
    elif(op == 2):
        n = int(input("Digite a quantidade de pessoas que vão sair "))
        e.sai(n)
    elif(op == 3):
        e.sobe()
    elif(op == 4):
        e.desce()
    elif(op == 5):
        capacidade = int(input("Digite a capacidade do elevador "))
        andares = int(input("Digite o total de andares "))
        e = e.inicializa(andares, capacidade)

        
        