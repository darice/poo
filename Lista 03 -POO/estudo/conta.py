from historico import Historico

class Conta:

    _total_contas = 0

    def __init__(self,numero,cliente,saldo = 0):
        self.historico = Historico()
        self._numero = numero
        self.cliente = cliente
        self.saldo = saldo
        Conta._total_contas += 1

    @staticmethod
    def get_total_contas():
        return Conta._total_contas

    @property
    def get_numero(self):
        return self._numero;

    @get_numero.setter
    def set_numero(self,novo_numero):
        self._numero = novo_numero;

    def depositar(self, valor):
        self.historico.adicionar_transacao('depositou saldo {}'.format(valor))
        self.saldo += valor

    def sacar (self, valor):
        self.historico.adicionar_transacao('sacou saldo', valor)
        self.saldo -= valor

    def extrato(self):
        self.cliente.imprimirDados()
        self.historico.imprimir_transacoes()
        print("Saldo:",self.saldo)

    def transfere(self, contaDestino, valor):
        if self.saldo < valor:
            print("Saldo insuficiente para realizar a transferencia")
        else:
            self.historico.adicionar_transacao('transferiu {}'.format(valor))
            contaDestino.historico.adicionar_transacao('recebeu saldo {}'.format(valor))
            self.saldo -= valor
            contaDestino.saldo += valor
            print("Transferencia Ok")