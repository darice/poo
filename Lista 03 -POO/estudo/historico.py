from datetime import datetime

class Historico:
    def __init__(self):
        self.data_abertura = datetime.today()
        self.transacoes = []

    def adicionar_transacao(self, str):
        self.transacoes.append(str)

    def imprimir_transacoes(self):
        print('conta aberta em', self.data_abertura)
        print('transacoes:')
        for transacao in self.transacoes:
            print(transacao)

        