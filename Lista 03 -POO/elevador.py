class Elevador:

    __slots__ = ['_totalAndares', '_capacidade','_andarAtual', '_atualmente']

    def __init__(self,totalAndares, capacidade,andarAtual=0, atualmente=0):
        self._andarAtual = andarAtual
        self._totalAndares = totalAndares
        self._capacidade = capacidade
        self._atualmente = atualmente

    @property
    def andarAtual(self):
        return self._andarAtual

    @property
    def totalAndares(self):
        return self._totalAndares

    @totalAndares.setter
    def totalAndares(self, novoValor):
        self._totalAndares = novoValor

    @property
    def capacidade(self):
        return self._capacidade

    @capacidade.setter
    def capacidade(self, novoValor):
        self._capacidade = novoValor

    @property
    def atualmente(self):
        return self._atualmente

    def inicializa(self,totalAndares, capacidade):
        return Elevador(totalAndares, capacidade)
    
    def entra(self, qntPessoas5
    ):
        if(self._atualmente + qntPessoas > self._capacidade):
            print('capacidade insuficiente')
        else:
            self._atualmente += qntPessoas
        print("quantidade de pessoas no elevador:", self.atualmente)
            
    
    def sai(self, qntPessoas):
        if(self._atualmente - qntPessoas < 0):
            print('Impossivel sair mais gente do que tem')
        else:
            self._atualmente -= qntPessoas
        print("quantidade de pessoas no elevador:", self.atualmente)

    def sobe(self):
        if(self._andarAtual == self._totalAndares):
            print("Impossivel subir mais. Já está no último andar")
        else:
            self._andarAtual += 1
        print("andar atual:", self.andarAtual)

    def desce(self):
        if(self._andarAtual == 0):
            print("Impossível descer mais. Já está no terreo")
        else:
            self._andarAtual -= 1
        print("andar atual:", self.andarAtual)



