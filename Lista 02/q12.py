# 12. Uma pista de Kart permite 3 voltas para cada um de 5 corredores. Escreva um programa
# que leia todos os tempos em segundos e os guarde em um dicionário, onde a chave é o
# nome do corredor. Ao final diga de quem foi a melhor volta da prova e em que volta; e ainda
# quem foi o campeão. O campeão é o que tem a menor média de tempos.

listaDeTempos = []

for i in range(5):
    dicionario = {}
    nome = input("\nNome: ")
    dicionario[nome] = []
    for j in range(3):
        dicionario[nome].append(int(input("Tempo da volta:")))
    listaDeTempos.append(dicionario)

primeiro = True

for corredorDicionario in listaDeTempos:
    for nomeCorredor in corredorDicionario:
        somaTempo = corredorDicionario[nomeCorredor][0] + corredorDicionario[nomeCorredor][1] + corredorDicionario[nomeCorredor][2];
        if(primeiro == True or somaTempo < somaMenorTempo):
            somaMenorTempo = somaTempo
            corredorSomaMenorTempo = nomeCorredor;
        for volta in range(3):
            if(primeiro == True or corredorDicionario[nomeCorredor][volta]< menorTempo):
                menorTempo = corredorDicionario[nomeCorredor][volta]
                voltaMenorTempo = volta
                corredorMenortempo = nomeCorredor
                primeiro = False

print("A melhor volta foi do corredor", corredorMenortempo, "na volta", volta+1)

print("O corredor", corredorSomaMenorTempo, "foi o campeão")
        
        
            
