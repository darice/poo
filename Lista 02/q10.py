# Gere uma lista de 100 números inteiros de 1 até 100. Apenas usando as funções básicas
# (não use bibliotecas científicas tais como numpy, panda ou scipy). Calcule:
# a. Média
# b. Mediana (lembre-se, se o “n” é par ou ímpar)
# c. Variância
# d. Desvio Padrão

from random import randint

lista = []

for i in range(100):
    lista.append(randint(1,100))

mediana = lista[50] + lista[51];

media = 0
for item in lista:
    media += item
media /= 100

variancia = 0
for item in lista:
    variancia += (item-media)**2
variancia /= 100

desvioMedio = variancia**(1/2)

print("Media:",media)
print("Mediana:", mediana)
print("Variancia:", variancia)
print("Desvio médio:", desvioMedio)


