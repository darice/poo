class Pessoa:
    def __init__(self, nome, cpf, dataNascimento):
        self._nome = nome
        self._cpf = cpf
        self._dataNascimento = dataNascimento

    def imprimirPessoa(self):
        print("nome: %s\ncpf:%s\ndata de nascimento:%s" %(self._nome, self._cpf, self._dataNascimento))

    @property
    def getCPF(self):
        return self._cpf;