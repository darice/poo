from cliente import Cliente

class SeguroDeVida:
    def __init__(self, valorMensal, valorTotal):
        self._valorMensal = valorMensal
        self._valorTotal = valorTotal
    
    def criarSeguroDeVida(cpf, valorMensal, valorTotal):
        cliente = Cliente.retornaClientePorCPF(cpf)
        if(cliente != False):
            cliente.seguroDeVida.append(SeguroDeVida(valorMensal, valorTotal))
            print("Seguro de vida criado com sucesso")
    
    def valorMensalSoma(cliente):
        soma = 0
        seguros = Cliente.getSeguroDeVida(cliente)
        if(seguros != None):
            for seguro in seguros:
                soma += float(seguro._valorMensal)
            return soma
        return 0

    