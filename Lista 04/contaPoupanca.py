from conta import Conta
from cliente import Cliente
from seguroDeVida import SeguroDeVida
from datetime import datetime
from historico import Historico

class ContaPoupanca(Conta):
    def __init__(self):
        super().__init__()
        self._historicoTransacoes = []

    def criarContaPoupanca(cpf):
        cliente = Cliente.retornaClientePorCPF(cpf)
        if(cliente != False):
            if(cliente.contaPoupanca == False):
                cliente.contaPoupanca = ContaPoupanca()
                print("Conta criada com sucesso")
                Conta.getConta(cliente.contaPoupanca)
            else:
                print("Este cliente já possui uma conta poupanca cadastrada")
    
    def getSaldo(cliente):
        return float(cliente.contaPoupanca.saldo)

    def setSaldo(cliente, movimentacao):
        saldo = float(cliente.contaPoupanca.saldo)
        saldo = saldo + float(movimentacao)
        cliente.contaPoupanca.saldo = saldo      

    def adicionarTransacao(cliente,descricao, valor):
        cliente.contaPoupanca._historicoTransacoes.append(str(datetime.now()) + ' ' +descricao + str(valor))

    def saque(cliente, valor):
        if(ContaPoupanca.getSaldo(cliente) - float(valor) >= 0):
            ContaPoupanca.adicionarTransacao(cliente,"Saque no valor de R$", valor)
            valor = - float(valor)
            ContaPoupanca.setSaldo(cliente, valor)
            return True
        else:
            print("Saldo insuficiente")

    def deposito(cliente, valor):
        ContaPoupanca.setSaldo(cliente, valor)
        ContaPoupanca.adicionarTransacao(cliente,"Deposito no valor de R$", valor)
   
    def imprimirHistorico(cliente):
            print('historico de transacoes:')
            for historico in cliente.contaPoupanca._historicoTransacoes:
                print(historico)

    def retornaClientePorCPF(cpfBuscar):
        for pessoa in Cliente.clientes:
            cpf= pessoa.getCPF
            if(cpf == cpfBuscar):
                return pessoa
        print("Não há nenhum cliente cadastrado com esse CPF")
        return False

    def getContaPoupanca(self):
        return self.getConta()

    def getNumeroPoupanca(self):
        return self.getNumero()

    def exibirExtrato(cliente):
        Conta.getConta(cliente.contaPoupanca)