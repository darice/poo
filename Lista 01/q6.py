def ehPrimo(n):
    for i in range(2, n):
        if(n % i == 0):
            return False
    return True

a = int(input())
b = int(input())
temPrimo = False
for i in range(a,b+1):
    if(ehPrimo(i)):
        print(i)
        temPrimo = True
if(temPrimo == False):
    print("Não existe nenhum número primo dentro desse intervalo")